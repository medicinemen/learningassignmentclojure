Learning Assignment Clojure
===========================

Awesome that you want to come and develop in Clojure with us! Clojure is a bit
different from many other languages, so there will be a learning curve coming
your way. This assignment is intended to guide you through that. The idea is
that a junior Clojure dev can complete it in about five hours. Of course it will
take you more time, because of the learning that you need to do. Please come up
with a solution yourself. There are plenty of solutions out there on the
Internet, but copy-pasting those defeats the purpose of this assignment and will
probably backfire in the next phase. Remember, like Confucius taught us: it’s
not about the destination, it’s about the journey. Don't forget to have fun!

Assignment
----------

Your assignment is to implement the game [tic-tac-toe][tictactoe] (NL: “Boter,
kaas en eieren”). These are the requirements:

* The game has to be interactive and actually playable.
* A simple text-based terminal UI is good enough. Meaning: the game repeatedly
  asks a player what they want to play and prints the updated board.
* The game has to detect when somebody has won.
* The game has to tell who has won.
* The game has to tell why that person won (was it because of the forward
  diagonal? The second row?).
* There have to be unit tests for the game logic.
* Use git. Please don't push it to a remote, so that we can keep using this in
  the future for other candidates, but use git locally and send us your solution
  including the git history.

We are providing you with a starter project so that you don’t have to set
everything up from scratch.

Resources
---------

Of course you can use anything you can find. You can also contact us at any time
if you have questions. Here are some resources that we recommend:

* [“Simple made Easy”][simple-made-easy], talk by Rich Hickey about what makes
  functional programming and its constructs stand out. This one is important to
  get the right mindset.
* [ClojureScript Koans][cljskoans] and/or [Clojure Koans][cljkoans]: “exercises
  meant to initiate you to the mysteries of the Clojure language”. The
  ClojureScript one can be done in the browser, the Clojure one is more
  extensive.
* [Clojure for the Brave and True][bravetrue]: Clojure book geared towards
  beginners, free to read online.
* Best online [reference of Clojure core library][cljdocs].
* Tooling: Calva plugin in VSCode; please contact us to schedule a
  live-share session to get the fundamentals explained.
* Practicing your Clojure skills further can be done also on
  [Exercism][exercism].

About this Starter Project
--------------------------

Some things that you need on your computer:

* Clojure compiler and CLI Tools; see the [getting started][gettingstarted]
  instructions on the Clojure site for details about how to install those.
* [Visual Studio Code][vscode]
* [Calva][calva] VSCode plugin; see [user guide][calvauserguide]. This starter
  project has some configuration for Calva that we find useful, but you can of
  course change that to your liking.
* [Live Share][liveshare] so that we can better cooperate and help you with
  issues.

You can run the (terminal) application using this command:

```bash
clj -M:run-m
```

You can run the unit test like this:

```bash
clj -M:test:runner
```

You probably don't need to do either if you setup VSCode correctly. Please
schedule a pair programming session with us so that we can show you how to setup
and use Calva effectively.

Remember
--------

Have fun! "Der Weg ist das Ziel".

[tictactoe]: https://en.wikipedia.org/wiki/Tic-tac-toe
[simple-made-easy]: https://www.infoq.com/presentations/Simple-Made-Easy/
[cljskoans]: http://clojurescriptkoans.com
[cljkoans]: http://www.clojurekoans.com
[bravetrue]: https://www.braveclojure.com/foreword/
[cljdoc]: https://clojuredocs.org
[exercism]: https://exercism.io
[gettingstarted]: https://www.clojure.org/guides/getting_started
[vscode]: https://code.visualstudio.com
[calva]: https://marketplace.vscode.pro/calva
[calvauserguide]: https://calva.io
[liveshare]: https://docs.microsoft.com/en-us/visualstudio/liveshare/
[bitbucketfork]: https://support.atlassian.com/bitbucket-cloud/docs/fork-a-repository/
