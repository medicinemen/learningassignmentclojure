(ns tic-tac-toe.core
  (:gen-class))

(def initial-game-state 
  :todo)

(defn turn
  [state move] 
  :todo)

(defn read-game
  [] 
  [:todo])

(defn -main
  [& _]
  (println "Welcome to Tic Tac Toe")
  (reduce turn initial-game-state (read-game)))
